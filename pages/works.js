import { Container, Heading, SimpleGrid, Divider } from '@chakra-ui/react'
import Layout from '../components/layouts/article'
import Section from '../components/section'
import { WorkGridItem } from '../components/grid-item'

import thumbSolidTech from '../public/images/works/solidtech.png'
import thumbGoaw from '../public/images/works/goaw.png'
import thumbFlinters from '../public/images/works/flinters.jpeg'

const Works = () => (
  <Layout title="Works">
    <Container>
      <Heading as="h3" fontSize={20} mb={4}>
        Works
      </Heading>

      <SimpleGrid columns={[1, 1, 2]} gap={6}>
        <Section>
          <WorkGridItem
              // id="solidtech"
              title="Solid Tech., Jsc"
              thumbnail={thumbSolidTech}>
            {/*Vietnam startup*/}
          </WorkGridItem>
        </Section>
        <Section>
          <WorkGridItem
            // id="goaw"
            title="Goaw VietNam"
            thumbnail={thumbGoaw}
          >
            {/*Music recommendation app for iOS*/}
          </WorkGridItem>
        </Section>

        <Section delay={0.1}>
          <WorkGridItem
            id="flinters"
            title="Flinters VietNam"
            thumbnail={thumbFlinters}
          >
            {/*A video work generated with deep learning, imitating famous four*/}
            {/*painters like Van Gogh*/}
          </WorkGridItem>
        </Section>
        {/*<Section delay={0.1}>*/}
        {/*  <WorkGridItem id="menkiki" thumbnail={thumbGoaw} title="Menkiki">*/}
        {/*    An app that suggests ramen(noodle) shops based on a given photo of*/}
        {/*    the ramen you want to eat*/}
        {/*  </WorkGridItem>*/}
        {/*</Section>*/}
      </SimpleGrid>

      {/*<Section delay={0.2}>*/}
      {/*  <Divider my={6} />*/}

      {/*  <Heading as="h3" fontSize={20} mb={4}>*/}
      {/*    Collaborations*/}
      {/*  </Heading>*/}
      {/*</Section>*/}

      {/*<SimpleGrid columns={[1, 1, 2]} gap={6}>*/}
      {/*  <Section delay={0.3}>*/}
      {/*    <WorkGridItem*/}
      {/*      id="modetokyo"*/}
      {/*      thumbnail={thumbModeTokyo}*/}
      {/*      title="mode.tokyo"*/}
      {/*    >*/}
      {/*      The mode magazine for understanding to personally enjoy Japan*/}
      {/*    </WorkGridItem>*/}
      {/*  </Section>*/}
      {/*  <Section delay={0.3}>*/}
      {/*    <WorkGridItem id="styly" thumbnail={thumbStyly} title="Styly">*/}
      {/*      A VR Creative tools for fashion brands*/}
      {/*    </WorkGridItem>*/}
      {/*  </Section>*/}
      {/*</SimpleGrid>*/}

      {/*<Section delay={0.4}>*/}
      {/*  <Divider my={6} />*/}

      {/*  <Heading as="h3" fontSize={20} mb={4}>*/}
      {/*    Old works*/}
      {/*  </Heading>*/}
      {/*</Section>*/}

      {/*<SimpleGrid columns={[1, 1, 2]} gap={6}>*/}
      {/*  <Section delay={0.5}>*/}
      {/*    <WorkGridItem id="pichu2" thumbnail={thumbPichu2} title="Pichu*Pichu">*/}
      {/*      Twitter client app for iPhone Safari*/}
      {/*    </WorkGridItem>*/}
      {/*  </Section>*/}
      {/*  <Section delay={0.5}>*/}
      {/*    <WorkGridItem*/}
      {/*      id="freedbtagger"*/}
      {/*      thumbnail={thumbFreeDBTagger}*/}
      {/*      title="freeDBTagger"*/}
      {/*    >*/}
      {/*      Automatic audio file tagging tool using FreeDB for Windows*/}
      {/*    </WorkGridItem>*/}
      {/*  </Section>*/}
      {/*  <Section delay={0.6}>*/}
      {/*    <WorkGridItem id="amembo" thumbnail={thumbAmembo} title="Amembo">*/}
      {/*      P2P private file sharing tool with MSN Messenger integration for*/}
      {/*      Windows*/}
      {/*    </WorkGridItem>*/}
      {/*  </Section>*/}
      {/*</SimpleGrid>*/}
    </Container>
  </Layout>
)

export default Works
export { getServerSideProps } from '../components/chakra'
