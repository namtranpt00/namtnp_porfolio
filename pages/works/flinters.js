import {
  Box,
  Container,
  Badge,
  Link,
  List,
  ListItem,
  UnorderedList,
  Heading,
  Center
} from '@chakra-ui/react'
import { ExternalLinkIcon } from '@chakra-ui/icons'
import { Title, WorkImage, Meta } from '../../components/work'
import P from '../../components/paragraph'
import Layout from '../../components/layouts/article'

const Work = () => (
  <Layout title="The four painters">
    <Container>
      <Title>
        Flinters VietNam <Badge>07/2021 - now</Badge>
      </Title>
      <P>
        Septeni Group expands into Digital Marketing, Media Platform business. FLINTERS develops advertising products and provides data usage solutions.
      </P>
      <List ml={4} my={4}>
        <ListItem>
          <Meta>Previous position</Meta>
          <span>Developer</span>
        </ListItem>
        <ListItem>
          <Meta>Position</Meta>
          <span>Infrastructure</span>
        </ListItem>

        {/*<ListItem>*/}
        {/*  <Meta>Blogpost</Meta>*/}
        {/*  <Link href="https://archive.craftz.dog/blog.odoruinu.net/2015/12/23/the-four-painters-a-video-work-created-with-deep-learning/">*/}
        {/*    The four painters: A Video Work Created with Deep Learning{' '}*/}
        {/*    <ExternalLinkIcon mx="2px" />*/}
        {/*  </Link>*/}
        {/*</ListItem>*/}
        {/*<ListItem>*/}
        {/*  <Meta>Blogpost</Meta>*/}
        {/*  <Link href="https://archive.craftz.dog/blog.odoruinu.net/2015/12/19/created-movie-with-deep-learning/">*/}
        {/*    Deep Learningを使って映像作品を作った*/}
        {/*    <ExternalLinkIcon mx="2px" />*/}
        {/*  </Link>*/}
        {/*</ListItem>*/}
      </List>

      <Heading as="h4" fontSize={16} my={6}>
        <Center>Project/Teammate</Center>
      </Heading>

      <UnorderedList my={4}>
        <ListItem>
          {/*<Link href="https://news.ycombinator.com/item?id=10782289">*/}
          {/*  <Badge mr={2}>CTX</Badge>*/}
            CTX
            {/*<ExternalLinkIcon mx="2px" />*/}
          {/*</Link>*/}
        </ListItem>

        <ListItem>
          {/*<Link href="https://gigazine.net/news/20151224-deep-learning-four-painters/">*/}
            {/*<Badge mr={2}>Infra team</Badge>*/}
            Infra team
            {/*four painters」*/}
            {/*<ExternalLinkIcon mx="2px" />*/}
          {/*</Link>*/}
        </ListItem>
      </UnorderedList>

      {/*<Box>*/}
      {/*  <iframe*/}
      {/*    src="https://player.vimeo.com/video/146373709"*/}
      {/*    width="100%"*/}
      {/*    height="400"*/}
      {/*    frameBorder="0"*/}
      {/*    allowFullScreen*/}
      {/*  />*/}
      {/*</Box>*/}

      <WorkImage
        src="/images/works/ctxteam.jpg"

      />
      <WorkImage src="/images/works/infrateam.png"/>
      {/*<WorkImage src="/images/works/the-four-painters_02.jpg" alt="walknote" />*/}
    </Container>
  </Layout>
)

export default Work
export { getServerSideProps } from '../../components/chakra'
