import NextLink from 'next/link'
import {
  Link,
  Container,
  Heading,
  Box,
  Image,
  SimpleGrid,
  Button,
  List,
  ListItem,
  Icon,
  useColorModeValue, UnorderedList
} from '@chakra-ui/react'
import { ChevronRightIcon } from '@chakra-ui/icons'
import Paragraph from '../components/paragraph'
import { BioSection, BioYear } from '../components/bio'
import Layout from '../components/layouts/article'
import Section from '../components/section'
import { GridItem } from '../components/grid-item'
import { IoLogoTwitter, IoLogoInstagram, IoLogoGithub } from 'react-icons/io5'

const Home = () => (
  <Layout>
    <Container>
      <Box
        borderRadius="lg"
        mb={6}
        p={3}
        textAlign="center"
        bg={useColorModeValue('whiteAlpha.500', 'whiteAlpha.200')}
      >
        Hello, I'm a devops engineer!
      </Box>

      <Box display={{ md: 'flex' }}>
        <Box flexGrow={1}>
          <Heading as="h2" variant="page-title">
            Tran Nguyen Phuong Nam
          </Heading>
          <p>Last year student - University of Enginnering and Technology</p>
        </Box>
        <Box
          flexShrink={0}
          mt={{ base: 4, md: 0 }}
          ml={{ md: 6 }}
          textAlign="center"
        >
          <Image
            borderColor="whiteAlpha.800"
            borderWidth={2}
            borderStyle="solid"
            maxWidth="100px"
            display="inline-block"
            borderRadius="full"
            src="/images/avt.jpg"
            alt="Profile image"
          />
        </Box>
      </Box>
      <Section delay={0.1}>
        <Heading as="h3" variant="section-title">
          Bio
        </Heading>
        <BioSection>
          <BioYear>21-09-2000</BioYear>
          Born in Phu Tho province, Viet Nam.
        </BioSection>
        <BioSection>
          <BioYear>2016 - 2019</BioYear>
          Studied at Hung Vuong high school for the gifted.
        </BioSection>
        <BioSection>
          <BioYear>2019 - now    </BioYear>
          Study at University of Engineering and Technology
          {/*<BioSection>*/}
          {/*  <BioYear>5/2020 - 8/2020</BioYear>*/}
          {/*  PHP inrtern at {''}*/}
          {/*  <Link href="https://solidtech.vn/" target="_blank">*/}
          {/*    Solid Tech Co*/}
          {/*  </Link>*/}
          {/*</BioSection>*/}
          {/*<BioSection>*/}
          {/*  <BioYear>10/2020 - 12/2020</BioYear>*/}
          {/*      Graduate Laravel programing course at Codestar academy*/}
          {/*</BioSection>*/}
          {/*<BioSection>*/}
          {/*  <BioYear>3/2021 - 8/2021</BioYear>*/}
          {/*  PHP intern at {''}*/}
          {/*  <Link href="https://goaw.net/" target="_blank">*/}
          {/*    Goaw Vietnam*/}
          {/*  </Link>*/}
          {/*</BioSection>*/}
          {/*<BioSection>*/}
          {/*  <BioYear>8/2021 - now</BioYear>*/}
          {/*  Full-stack fresher {''}*/}
          {/*  <Link href="https://www.flinters.vn/" target="_blank">*/}
          {/*    Flinters VietNam*/}
          {/*  </Link>*/}
          {/*</BioSection>*/}
        </BioSection>

      </Section>

      <Section delay={0.2}>
        <Heading as="h3" variant="section-title">
          Work
        </Heading>
        <Paragraph>
          Let's take a look at my projects and experiences
        </Paragraph>
        <Box align="center" my={4}>
          <NextLink href="/works">
            <Button rightIcon={<ChevronRightIcon />} colorScheme="teal">
              My portfolio
            </Button>
          </NextLink>
        </Box>
      </Section>

      <Section delay={0.3}>
        <Heading as="h3" variant="section-title">
          Hobbies
        </Heading>
        <UnorderedList my={4}>
          <ListItem>
            One piece
          </ListItem>
          <ListItem>
            Badminton
          </ListItem>
          <ListItem>
            Thrilling Game
          </ListItem>
        </UnorderedList>

      </Section>



      {/*<Section delay={0.3}>*/}
      {/*  <Heading as="h3" variant="section-title">*/}
      {/*    Some archivements*/}
      {/*  </Heading>*/}
      {/*  <BioSection>*/}
      {/*    <BioYear>12/2019</BioYear>*/}
      {/*    UET Encourage academic scholarship*/}
      {/*    <br />*/}
      {/*    &emsp; &nbsp;Certificate of merit for good students*/}
      {/*  </BioSection>*/}
      {/*  <BioSection>*/}
      {/*    <BioYear>12/2020</BioYear>*/}
      {/*    UET Encourage academic scholarship*/}
      {/*    <br />*/}
      {/*    &emsp; &nbsp;Certificate of merit for good students*/}
      {/*  </BioSection>*/}
      {/*  <BioSection>*/}
      {/*    <BioYear>5/2021</BioYear>*/}
      {/*    UET Encourage academic scholarship*/}
      {/*  </BioSection>*/}
      {/*  <BioSection>*/}
      {/*    <BioYear>12/2021</BioYear>*/}
      {/*    UET Encourage academic scholarship*/}
      {/*    <br />*/}
      {/*    &emsp; &nbsp;Certificate of merit for excelent students*/}
      {/*  </BioSection>*/}

      {/*</Section>*/}

      <Section delay={0.3}>



      </Section>
    </Container>
  </Layout>
)

export default Home
export { getServerSideProps } from '../components/chakra'
